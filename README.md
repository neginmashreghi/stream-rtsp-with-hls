### Discription
Stream RTSP with HLS react application.

---

### HTTP Live Streaming (HLS)
HLS is a media streaming protocol for delivering visual and audio media to viewers over the internet.

---

### Stream RTSP with HTTP Live Streaming (HLS) 

#### step 1

Use docker image that starts a container that can take existing streams (e.g.: rtsp streams from your security cameras) and use FFMPEG to convert it into HLS streams and drop them in a folder that Nginx is serving.

- [streamer](https://github.com/gihad/streamer) 

##### Run with docker

The parameters need to be passed in as a single environment variable called PARAMETERS separated by spaces in the following format:

`INPUT_STREAM_1 STREAM_1_NAME INPUT_STREAM_2 STREAM_2_NAME`

Input followed by stream name. In the example above 2 streams will be created but the program supports more than 2

`
docker run -e PARAMETERS="rtsp://username:password@192.168.1.183:554/cam/realmonitor?channel=1&subtype=1 frontyard rtsp://username:password@192.168.1.183:554/cam/realmonitor?channel=2&subtype=1 driveway" -v /tmp/stream:/tmp/stream -p 8080:80 gihad/streamer
`

##### Accessing the stream(s)

The streams will be accessible on port 8080 and the resource will be the chosen stream name suffixed with .m3u8.

Example: if the address of the host is `192.168.1.100` and the name of the stream passed as a parameter was `driveway` you will find the HLS steam at `http://192.168.1.100:8080/driveway.m3u8`

### step 2

Use HLS player to display stream 

- [react-hls-player](https://www.npmjs.com/package/react-hls-player)

---

### Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.




