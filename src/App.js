import React from 'react';
import ReactHlsPlayer from 'react-hls-player';

function App() {
  return (
    <div className="App">
      <h1>React HLS Player</h1>
     <ReactHlsPlayer
    url='HLS addres'
    autoplay={false}
    controls={true}
    width={500}
    height={375}
/>

    </div>
  );
}

export default App;
